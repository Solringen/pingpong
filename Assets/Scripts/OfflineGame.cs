﻿using System.Collections;
using UnityEngine;

namespace PingPong
{
	public class OfflineGame : MonoBehaviour, Game
	{
#pragma warning disable 0649
		[SerializeField] private BallFactory ballFactory;
		[SerializeField] private RacketFactory racketFactory;
#pragma warning restore 0649

		private Ball ball;

		public void StartGame() {
			racketFactory.CreateForPlayer1();
			racketFactory.CreateForPlayer2();

			PutBallInGame();
		}

		public void PlayerScored(int player, int score) {
			StartCoroutine(StartNewRound());
		}

		private IEnumerator StartNewRound() {
			yield return new WaitForSeconds(1f);
			PutBallInGame();
		}

		private void PutBallInGame() {
			if (ball != null) {
				Destroy(ball.gameObject);
			}
			ball = ballFactory.CreateRandom();
			ball.Launch();
		}
	}
}