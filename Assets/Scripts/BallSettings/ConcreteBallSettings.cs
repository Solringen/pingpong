﻿using UnityEngine;

namespace PingPong
{
	[CreateAssetMenu(fileName = "Ball Settings", menuName = "Settings/Concrete Ball")]
	public class ConcreteBallSettings : BallSettings
	{
#pragma warning disable 0649
		[SerializeField] private float speed = 20;
		[SerializeField] private float radius = 1;
#pragma warning restore 0649

		public override float Speed => speed;
		public override float Radius => radius;
	}
}