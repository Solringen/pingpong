﻿using UnityEngine;

namespace PingPong
{
	[CreateAssetMenu(fileName = "Ball Settings", menuName = "Settings/Random Ball")]
	public class RandomBallSettings : BallSettings
	{
#pragma warning disable 0649
		[SerializeField] private float minSpeed = 20;
		[SerializeField] private float maxSpeed = 30;

		[SerializeField] private float minRadius = 1;
		[SerializeField] private float maxRadius = 2;
#pragma warning restore 0649

		public override float Speed => Random.Range(minSpeed, maxSpeed);
		public override float Radius => Random.Range(minRadius, maxRadius);
	}
}