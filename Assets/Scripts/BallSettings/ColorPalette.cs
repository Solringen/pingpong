﻿using UnityEngine;

namespace PingPong
{
	[CreateAssetMenu(fileName = "Color Palette", menuName = "Settings/Color Palette")]
	public class ColorPalette : ScriptableObject
	{
		public Color[] colors;
	}
}