﻿using UnityEngine;

namespace PingPong
{
	public abstract class BallSettings : ScriptableObject
	{
		public abstract float Speed { get; }
		public abstract float Radius { get; }
	}
}