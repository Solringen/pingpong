﻿using System;
using PingPong.Persistence;
using UnityEngine;

namespace PingPong
{
	public class Referee : MonoBehaviour
	{
#pragma warning disable 0649
		[SerializeField] private Goal goal1;
		[SerializeField] private Goal goal2;
#pragma warning restore 0649

		private int score1;
		private int score2;
		private int highscore;
		private SavedHighscore savedHighscore = new SavedHighscore();

		public int Highscore => highscore;

		public event Action<int> Score1Changed = delegate { };
		public event Action<int> Score2Changed = delegate { };
		public event Action<int> HighscoreChanged = delegate { };

		public void Player1Scored() {
			score1++;
			Score1Changed(score1);

			if (score1 > highscore) {
				highscore = score1;
				HighscoreChanged(highscore);
			}
		}

		public void Player2Scored() {
			score2++;
			Score2Changed(score2);

			if (score2 > highscore) {
				highscore = score2;
				HighscoreChanged(highscore);
			}
		}

		private void Awake() {
			goal1.Scored += Player2Scored;
			goal2.Scored += Player1Scored;
			highscore = savedHighscore.Load();
		}

		private void OnDestroy() {
			savedHighscore.Save(highscore);
		}
	}
}