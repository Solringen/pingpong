﻿
namespace PingPong
{
	public interface Game
	{
		void StartGame();
		void PlayerScored(int player, int score);
	}
}