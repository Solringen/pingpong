﻿using PingPong.Persistence;
using UnityEngine;

namespace PingPong
{
	public class BallFactory : MonoBehaviour
	{
#pragma warning disable 0649
		[SerializeField] private Ball originalBall;
		[SerializeField] private Vector3 spawnPoint;
		[SerializeField] private BallSettings[] ballSettings;
#pragma warning restore 0649

		private Color ballColor;

		public Ball CreateRandom() {
			int index = Random.Range(0, ballSettings.Length);
			BallSettings settings = ballSettings[index];
			return Create(settings.Speed, settings.Radius);
		}

		public Ball Create(float speed, float radius) {
			Ball ball = Instantiate(originalBall, spawnPoint, Quaternion.identity);

			ball.ChangeSpeed(speed);
			ball.ChangeRadius(radius);
			ball.ChangeColor(ballColor);
			return ball;
		}

		private void Awake() {
			SavedBallColor savedBallColor = new SavedBallColor();
			ballColor = savedBallColor.Load();
		}
	}
}