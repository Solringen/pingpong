﻿using UnityEngine;

namespace PingPong
{
	public class Racket : MonoBehaviour
	{
#pragma warning disable 0649
		[SerializeField] private float speed = 10f;
#pragma warning restore 0649

		private GameField gameField;

		public float Width => transform.localScale.x;
		public float Height => transform.localScale.y;

		public void Initialize(GameField gameField, Color color) {
			this.gameField = gameField;

			SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
			spriteRenderer.color = color;
		}

		public void MoveLeft() {
			float targetX = transform.position.x - speed * Time.deltaTime;
			SetPositionClamped(targetX);
		}

		public void MoveRight() {
			float targetX = transform.position.x + speed * Time.deltaTime;
			SetPositionClamped(targetX);
		}

		public void FollowX(float targetX) {
			float dx = targetX - transform.position.x;
			float step = speed * Time.deltaTime;
			dx = Mathf.Clamp(dx, -step, step);
			SetPositionClamped(transform.position.x + dx);
		}

		private void SetPositionClamped(float x) {
			x = Mathf.Clamp(x, gameField.PlayableArea.xMin + Width * 0.5f, gameField.PlayableArea.xMax - Width * 0.5f);
			transform.position = new Vector3(x, transform.position.y, transform.position.z);
		}
	}
}