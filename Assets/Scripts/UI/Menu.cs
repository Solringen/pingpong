﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace PingPong.UI
{
	public class Menu : MonoBehaviour
	{
#pragma warning disable 0649
		[SerializeField] private Button continueBtn;
		[SerializeField] private Button mainMenuBtn;
#pragma warning restore 0649

		public bool Showed => gameObject.activeSelf;

		public void Open() {
			gameObject.SetActive(true);
			if (!PhotonNetwork.connected) {
				Time.timeScale = 0;
			}
		}

		public void Close() {
			gameObject.SetActive(false);
			Time.timeScale = 1;
		}

		private void ToMainMenu() {
			SceneManager.LoadScene("MainMenu");
		}

		private void Awake() {
			continueBtn.onClick.AddListener(Close);
			mainMenuBtn.onClick.AddListener(ToMainMenu);
		}

		private void OnDestroy() {
			Time.timeScale = 1;
		}
	}
}