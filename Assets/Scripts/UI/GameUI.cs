﻿using TMPro;
using UnityEngine;

namespace PingPong.UI
{
	public class GameUI : MonoBehaviour
	{
#pragma warning disable 0649
		[SerializeField] private TextMeshProUGUI score1;
		[SerializeField] private TextMeshProUGUI score2;
		[SerializeField] private TextMeshProUGUI highscore;
		[SerializeField] private Menu menu;
#pragma warning restore 0649

		public void UpdateScore1(int value) {
			score1.text = value.ToString();
		}

		public void UpdateScore2(int value) {
			score2.text = value.ToString();
		}

		public void UpdateHighscore(int value) {
			highscore.text = value.ToString();
		}

		private void Update() {
			if (Input.GetKeyDown(KeyCode.Escape)) {
				if (menu.Showed) {
					menu.Close();
				}
				else {
					menu.Open();
				}
			}
		}
	}
}