﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace PingPong.MainMenu
{
	public class FindMatchDialog : MonoBehaviour
	{
#pragma warning disable 0649
		[SerializeField] private Button cancel;
#pragma warning restore 0649

		public event Action Closed = delegate { };

		private void Awake() {
			cancel.onClick.AddListener(OnCancelClick);
		}

		private void OnCancelClick() {
			gameObject.SetActive(false);
			Closed();
		}

		private void Update() {
			if (Input.GetKeyDown(KeyCode.Escape)) {
				OnCancelClick();
			}
		}
	}
}