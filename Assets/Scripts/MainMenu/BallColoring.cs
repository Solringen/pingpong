﻿using PingPong.Persistence;
using UnityEngine;
using UnityEngine.UI;

namespace PingPong.MainMenu
{
	public class BallColoring : MonoBehaviour
	{
#pragma warning disable 0649
		[SerializeField] private Image ball;
		[SerializeField] private ColorPalette palette;
		[SerializeField] private ColorItem template;
		[SerializeField] private RectTransform content;
		[SerializeField] private Button backBtn;
#pragma warning restore 0649

		private readonly SavedBallColor savedBallColor = new SavedBallColor();

		public void Show() {
			gameObject.SetActive(true);
		}

		private void BuildPalette() {
			foreach (var color in palette.colors) {
				var item = Instantiate(template, content);
				item.SetColor(color);
				item.Selected += OnColorChanged;
			}
			template.gameObject.SetActive(false);
		}

		private void OnColorChanged(Color color) {
			ball.color = color;
		}

		private void OnBackClick() {
			gameObject.SetActive(false);
			savedBallColor.Save(ball.color);
		}

		private void Awake() {
			backBtn.onClick.AddListener(OnBackClick);
			BuildPalette();
			ball.color = savedBallColor.Load();
		}

		private void Update() {
			if (Input.GetKeyDown(KeyCode.Escape)) {
				OnBackClick();
			}
		}
	}
}