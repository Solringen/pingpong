﻿using PingPong.Network;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace PingPong.MainMenu
{
	public class MainMenu : MonoBehaviour
	{
#pragma warning disable 0649
		[SerializeField] private Button multiplayer;
		[SerializeField] private Button offline;
		[SerializeField] private Button settings;
		[SerializeField] private BallColoring ballColoring;
		[SerializeField] private FindMatchDialog findDialog;
		[SerializeField] private NetworkMatch networkMatch;
#pragma warning restore 0649

		private void Awake() {
			multiplayer.onClick.AddListener(OnMultiplayerClick);
			offline.onClick.AddListener(OnOfflineClick);
			settings.onClick.AddListener(OnSettingsClick);

			findDialog.Closed += networkMatch.StopSearch;
		}

		private void OnMultiplayerClick() {
			findDialog.gameObject.SetActive(true);
			networkMatch.Find();
		}

		private void OnOfflineClick() {
			SceneManager.LoadScene("Playground");
		}

		private void OnSettingsClick() {
			ballColoring.Show();
		}
	}
}