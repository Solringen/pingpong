﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace PingPong.MainMenu
{
	public class ColorItem : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
	{
#pragma warning disable 0649
		[SerializeField] private Image image;
#pragma warning restore 0649

		public event Action<Color> Selected = delegate { };

		public void SetColor(Color color) {
			image.color = color;
		}

		void IPointerDownHandler.OnPointerDown(PointerEventData eventData) { }

		void IPointerUpHandler.OnPointerUp(PointerEventData eventData) {
			if (!eventData.dragging) {
				Selected(image.color);
			}
		}
	}
}