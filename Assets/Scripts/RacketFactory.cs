﻿using UnityEngine;

namespace PingPong
{
	public class RacketFactory : MonoBehaviour
	{
#pragma warning disable 0649
		[SerializeField] private Racket originalRacket;
		[SerializeField] private Color player1Color;
		[SerializeField] private Color player2Color;
		[SerializeField] private GameField gameField;
#pragma warning restore 0649

		public Racket CreateForPlayer1() {
			float yPos = gameField.PlayableArea.yMin + originalRacket.Height * 0.5f;
			return Create(yPos, player1Color, "Player 1");
		}

		public Racket CreateForPlayer2() {
			float yPos = gameField.PlayableArea.yMax - originalRacket.Height * 0.5f;
			return Create(yPos, player2Color, "Player 2");
		}

		private Racket Create(float yPos, Color color, string name) {
			Racket racket = Instantiate(originalRacket, new Vector3(0, yPos, 0), Quaternion.identity);
			racket.Initialize(gameField, color);
			racket.gameObject.name = name;
			return racket;
		}
	}
}