﻿using UnityEngine;

namespace PingPong
{
	[RequireComponent(typeof(Rigidbody2D))]
	public class Ball : MonoBehaviour
	{
		[SerializeField]
		private float speed = 20f;

		private new Rigidbody2D rigidbody;
		private new CircleCollider2D collider;
		private SpriteRenderer spriteRenderer;

		public float Speed => speed;
		public float Radius => transform.localScale.x;

		public event System.Action<Vector2> Bounced = delegate { };

		public void Launch() {
			rigidbody.velocity = RandomDirection() * speed;
		}

		public void ChangeColor(Color color) {
			spriteRenderer.color = color;
		}

		public void ChangeSpeed(float speed) {
			this.speed = speed;
		}

		public void ChangeRadius(float radius) {
			transform.localScale = new Vector3(radius, radius, 1f);
		}

		public void DisableSimulation() {
			rigidbody.simulated = false;
		}

		public void DisableCollider() {
			collider.enabled = false;
		}

		private Vector2 RandomDirection() {
			float yDir = Random.value > 0.5f ? 1f : -1f;
			float xDir = Random.Range(-0.7f, 0.7f);
			return new Vector2(xDir, yDir).normalized;
		}

		private void OnCollisionEnter2D(Collision2D collision) {
			if (collision.transform.TryGetComponent(out Racket racket)) {
				float dx = transform.position.x - racket.transform.position.x;
				int yDir = transform.position.y > 0 ? -1 : 1;
				Vector2 direction = new Vector2(dx / (racket.Width * 0.5f), yDir).normalized;

				rigidbody.velocity = direction * speed;
			}

			Bounced(collision.contacts[0].point);
		}

		private void Awake() {
			rigidbody = GetComponent<Rigidbody2D>();
			collider = GetComponent<CircleCollider2D>();
			spriteRenderer = GetComponent<SpriteRenderer>();
		}
	}
}