﻿using UnityEngine;

namespace PingPong
{
	public class GameField : MonoBehaviour
	{
#pragma warning disable 0649
		[SerializeField] private Goal goal1;
		[SerializeField] private Goal goal2;
		[SerializeField] private Wall leftWall;
		[SerializeField] private Wall rightWall;
		[SerializeField] private float wallWidth = 5;
#pragma warning restore 0649

		public Rect PlayableArea { get; private set; }

		private new Camera camera;
		private float height;

		public void AdjustSize(float aspectRatio) {
			float newWidth = height * aspectRatio;
			PlayableArea = new Rect(-newWidth * 0.5f + wallWidth, -height * 0.5f, newWidth - wallWidth * 2, height);

			Vector2 goalSize = new Vector2(newWidth, 1f);
			goal1.Setup(PlayableArea.yMin, goalSize);
			goal2.Setup(PlayableArea.yMax, goalSize);

			float curWidth = height * camera.aspect;
			float addWallWidth = (curWidth - newWidth) * 0.5f;

			Vector2 wallSize = new Vector2(wallWidth + addWallWidth, height);
			leftWall.Setup(-newWidth * 0.5f - addWallWidth, wallSize);
			rightWall.Setup(newWidth * 0.5f + addWallWidth, wallSize);
		}

		private void Awake() {
			camera = Camera.main;
			height = camera.orthographicSize * 2;

			AdjustSize(camera.aspect);
		}
	}
}