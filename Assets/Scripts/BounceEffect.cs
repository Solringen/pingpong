﻿using UnityEngine;

namespace PingPong
{
	[RequireComponent(typeof(Ball))]
	[RequireComponent(typeof(AudioSource))]
	public class BounceEffect : MonoBehaviour
	{
		private Ball ball;
		private AudioSource audioSource;

		private void Awake() {
			ball = GetComponent<Ball>();
			audioSource = GetComponent<AudioSource>();
			ball.Bounced += OnBounced;
		}

		private void OnBounced(Vector2 hitPoint) {
			audioSource.Play();
		}
	}
}