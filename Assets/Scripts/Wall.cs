﻿using UnityEngine;

namespace PingPong
{
	public class Wall : MonoBehaviour
	{
#pragma warning disable 0649
		[SerializeField] private BoxCollider2D boxCollider;
		[SerializeField] private SpriteRenderer spriteRenderer;
#pragma warning restore 0649

		public void Setup(float xPos, Vector2 size) {
			int sideSign = xPos < 0 ? -1 : 1;
			transform.position = new Vector3(xPos - size.x * 0.5f * sideSign, 0, 0);

			boxCollider.size = size;
			spriteRenderer.size = size;
		}
	}
}