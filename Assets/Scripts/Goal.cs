﻿using System;
using UnityEngine;

namespace PingPong
{
	public class Goal : MonoBehaviour
	{
		public event Action Scored = delegate { };

		public void Setup(float yPos, Vector2 size) {
			transform.position = new Vector3(0, yPos, 0);

			BoxCollider2D collider = GetComponent<BoxCollider2D>();
			collider.size = size;
		}

		private void OnTriggerEnter2D(Collider2D collision) {
			if (collision.TryGetComponent(out Ball ball)) {
				ball.DisableCollider();
				Scored();
			}
		}
	}
}