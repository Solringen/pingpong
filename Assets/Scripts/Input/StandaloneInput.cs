﻿using UnityEngine;

namespace PingPong
{
	public class StandaloneInput : MonoBehaviour
	{
#pragma warning disable 0649
		[SerializeField] private KeyCode leftKey;
		[SerializeField] private KeyCode rightKey;
#pragma warning restore 0649

		private Racket racket;

		private void Awake() {
			racket = GetComponent<Racket>();
		}

		private void Update() {
			if (Input.GetKey(leftKey)) {
				racket.MoveLeft();
			}
			if (Input.GetKey(rightKey)) {
				racket.MoveRight();
			}
		}
	}
}