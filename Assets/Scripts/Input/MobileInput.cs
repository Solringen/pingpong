﻿using UnityEngine;

namespace PingPong
{
	public class MobileInput : MonoBehaviour
	{
		private Racket racket;
		private new Camera camera;

		private int mySide;
		private int fingerId = -1;

		private void Awake() {
			racket = GetComponent<Racket>();
			camera = Camera.main;
			mySide = racket.transform.position.y > 0 ? 1 : -1;
		}

		private void Update() {
			for (int i = 0; i < Input.touchCount; i++) {
				Touch touch = Input.GetTouch(i);

				if (InMyArea(touch.position)) {
					if (touch.phase == TouchPhase.Began) {
						fingerId = touch.fingerId;
						Debug.Log($"Touch[{touch.fingerId}] at {touch.position}");
					}

					if (touch.fingerId == fingerId) {
						HandleMyTouch(touch);
					}
				}
			}
		}

		private void HandleMyTouch(Touch touch) {
			if (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary) {
				Vector3 worldPos = camera.ScreenToWorldPoint(touch.position);
				racket.FollowX(worldPos.x);
			}
			else if (touch.phase == TouchPhase.Canceled || touch.phase == TouchPhase.Canceled) {
				fingerId = -1;
			}
		}

		private bool InMyArea(Vector2 screenPos) {
			if (screenPos.y > Screen.height * 0.5f) {
				return mySide == 1;
			}
			else {
				return mySide == -1;
			}
		}
	}
}