﻿using UnityEngine;

namespace PingPong.Persistence
{
	public class SavedHighscore
	{
		private const string highscoreKey = "Highscore";

		public int Load() {
			return PlayerPrefs.GetInt(highscoreKey);
		}

		public void Save(int score) {
			PlayerPrefs.SetInt(highscoreKey, score);
		}
	}
}