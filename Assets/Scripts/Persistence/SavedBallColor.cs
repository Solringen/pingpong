﻿using UnityEngine;

namespace PingPong.Persistence
{
	public class SavedBallColor
	{
		private const string ballColorKey = "BallColor";

		public Color Load() {
			if (!PlayerPrefs.HasKey(ballColorKey)) {
				return Color.white;
			}

			string htmlColor = "#" + PlayerPrefs.GetString(ballColorKey);
			ColorUtility.TryParseHtmlString(htmlColor, out Color ballColor);
			return ballColor;
		}

		public void Save(Color color) {
			PlayerPrefs.SetString(ballColorKey, ColorUtility.ToHtmlStringRGBA(color));
		}
	}
}