﻿using System.Collections;
using System.Collections.Generic;
using Photon;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PingPong.Network
{
	public class NetworkGame : PunBehaviour, Game, IPunObservable
	{
#pragma warning disable 0649
		[SerializeField] private BallFactory ballFactory;
		[SerializeField] private RacketFactory racketFactory;
		[SerializeField] private GameField gameField;
		[SerializeField] private Referee referee;
#pragma warning restore 0649

		private Ball ball;
		private Racket myRacket;
		private Racket otherRacket;
		private readonly InterpolatedPosition ballInterpolatedPos = new InterpolatedPosition();

		public void StartGame() {
			PhotonNetwork.sendRate = 60;
			PhotonNetwork.sendRateOnSerialize = 60;

			if (PhotonNetwork.isMasterClient) {
				// wait for receiving screen resolution of other player to start the game
			}
			else {
				photonView.RPC(nameof(MyScreenSize), PhotonTargets.MasterClient, Screen.width, Screen.height);
			}

			CreateMyRacket();
		}

		public void PlayerScored(int player, int score) {
			StartCoroutine(StartNewRound());
			photonView.RPC(nameof(PlayerWasScored), PhotonTargets.Others, player);
		}

		private IEnumerator StartNewRound() {
			yield return new WaitForSeconds(1f);
			PutBallInGame();
		}

		private void PutBallInGame() {
			if (ball != null) {
				Destroy(ball.gameObject);
			}
			ball = ballFactory.CreateRandom();
			ball.Launch();

			photonView.RPC(nameof(SpawnBall), PhotonTargets.Others, ball.Speed, ball.Radius);
		}

		private void CreateMyRacket() {
			if (PhotonNetwork.isMasterClient) {
				myRacket = racketFactory.CreateForPlayer1();
			}
			else {
				myRacket = racketFactory.CreateForPlayer2();
			}

			int viewId = PhotonNetwork.AllocateViewID();
			SetupRacket(myRacket, viewId);

			photonView.RPC(nameof(CreateRacket), PhotonTargets.Others, PhotonNetwork.isMasterClient ? 1 : 2, viewId);
		}

		private void SetupRacket(Racket racket, int viewId) {
			PhotonView view = racket.gameObject.AddComponent<PhotonView>();
			view.viewID = viewId;
			view.synchronization = ViewSynchronization.Unreliable;

			RacketNetView racketNetView = racket.gameObject.AddComponent<RacketNetView>();
			view.ObservedComponents = new List<Component>() { racketNetView };
		}

		private void Update() {
			if (ball != null && !PhotonNetwork.isMasterClient) {
				if (ballInterpolatedPos.TryCalculatePosition(out Vector2 ballPos)) {
					ball.transform.position = ballPos;
				}
			}
		}

		private void OnDestroy() {
			if (PhotonNetwork.connected) {
				PhotonNetwork.Disconnect();
			}
		}

		#region RPC

		[PunRPC]
		private void SpawnBall(float speed, float radius) {
			if (ball != null) {
				Destroy(ball.gameObject);
			}
			ball = ballFactory.Create(speed, radius);
			ball.DisableSimulation();
			ballInterpolatedPos.Clear();
		}

		[PunRPC]
		private void CreateRacket(int racketId, int viewId) {
			if (racketId == 1) {
				otherRacket = racketFactory.CreateForPlayer1();
			}
			else if (racketId == 2) {
				otherRacket = racketFactory.CreateForPlayer2();
			}
			SetupRacket(otherRacket, viewId);

			Destroy(otherRacket.gameObject.GetComponent<StandaloneInput>());
			Destroy(otherRacket.gameObject.GetComponent<MobileInput>());
		}

		[PunRPC]
		private void PlayerWasScored(int player) {
			if (player == 1) {
				referee.Player1Scored();
			}
			else if (player == 2) {
				referee.Player2Scored();
			}
		}

		[PunRPC]
		private void AdjustGameFieldSize(float aspectRatio) {
			gameField.AdjustSize(aspectRatio);
		}

		[PunRPC]
		private void MyScreenSize(int width, int height) {
			float otherAspect = (float)width / height;
			float myAspect = (float)Screen.width / Screen.height;

			if (myAspect < otherAspect) {
				photonView.RPC(nameof(AdjustGameFieldSize), PhotonTargets.Others, myAspect);
			}
			else if (myAspect > otherAspect) {
				gameField.AdjustSize(otherAspect);
			}

			PutBallInGame();
		}

		#endregion

		#region Photon Messages

		public override void OnPhotonPlayerDisconnected(PhotonPlayer other) {
			Debug.Log("Player disconnected");
			SceneManager.LoadScene("MainMenu");
		}

		public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
			if (ball == null) {
				return;
			}

			if (stream.isWriting) {
				Vector2 ballPos = ball.transform.position;
				stream.SendNext(ballPos);
			}
			else {
				Vector2 ballPos = (Vector2)stream.ReceiveNext();
				ballInterpolatedPos.AddPosition(ballPos, info.timestamp);
			}
		}

		#endregion
	}
}