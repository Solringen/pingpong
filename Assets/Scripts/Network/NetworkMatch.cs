﻿using Photon;
using UnityEngine;

namespace PingPong.Network
{
	public class NetworkMatch : PunBehaviour
	{
		public void Find() {
			Connect();
		}

		public void StopSearch() {
			PhotonNetwork.Disconnect();
		}

		private void Connect() {
			if (!PhotonNetwork.connected) {
				PhotonNetwork.ConnectUsingSettings("1.0");
			}
		}


		#region Photon Messages

		public override void OnConnectedToMaster() {
			Debug.Log("OnConnectedToMaster");
			PhotonNetwork.JoinOrCreateRoom("Room", new RoomOptions() { MaxPlayers = 2 }, TypedLobby.Default);
		}

		public override void OnJoinedRoom() {
			Debug.Log("OnJoinedRoom");
			if (PhotonNetwork.room.PlayerCount == 2) {
				PhotonNetwork.LoadLevel("Playground");
			}
		}

		public override void OnPhotonPlayerConnected(PhotonPlayer newPlayer) {
			Debug.Log("Player connected");
			if (PhotonNetwork.room.PlayerCount == 2) {
				PhotonNetwork.LoadLevel("Playground");
			}
		}

		#endregion
	}
}