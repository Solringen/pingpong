﻿using UnityEngine;

namespace PingPong.Network
{
	public struct PositionState
	{
		public readonly Vector2 pos;
		public readonly double timestamp;

		public PositionState(Vector2 pos, double timestamp) {
			this.pos = pos;
			this.timestamp = timestamp;
		}

		public override string ToString() {
			return $"({pos}: {timestamp})";
		}
	}
}