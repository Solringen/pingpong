﻿using UnityEngine;

namespace PingPong.Network
{
	public class InterpolatedPosition
	{
		private int stateCount;
		private PositionState[] states = new PositionState[20];

		private const float interpolationDelay = 0.12f;

		public void AddPosition(Vector2 position, double timestamp) {
			PositionState state = new PositionState(position, timestamp);
			for (int i = states.Length - 1; i > 0; i--) {
				states[i] = states[i - 1];
			}

			states[0] = state;

			stateCount++;
			if (stateCount > states.Length) {
				stateCount = states.Length;
			}
		}

		public bool TryCalculatePosition(out Vector2 position) {
			double time = LerpTime();

			if (states[0].timestamp > time) {
				for (int i = 0; i < stateCount; i++) {
					if (states[i].timestamp <= time || time == stateCount - 1) {
						PositionState s1 = states[i];
						PositionState s2 = states[Mathf.Max(i - 1, 0)];

						double length = s2.timestamp - s1.timestamp;
						double t = 0.0;
						if (length > 0.0001) {
							t = (time - s1.timestamp) / length;
						}

						position = Vector2.Lerp(s1.pos, s2.pos, (float)t);
						return true;
					}
				}
			}
			position = Vector2.zero;
			return false;
		}

		public void Clear() {
			stateCount = 0;
		}

		private double LerpTime() {
			return PhotonNetwork.time - interpolationDelay;
		}
	}
}