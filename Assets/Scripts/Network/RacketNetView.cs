﻿using UnityEngine;

namespace PingPong.Network
{
	public class RacketNetView : Photon.MonoBehaviour, IPunObservable
	{
		private readonly InterpolatedPosition interpolatedPosition = new InterpolatedPosition();

		private void Update() {
			if (!photonView.isMine) {
				if (interpolatedPosition.TryCalculatePosition(out Vector2 pos)) {
					transform.position = pos;
				}
			}
		}

		public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
			if (stream.isWriting) {
				Vector2 pos = transform.position;
				stream.SendNext(pos);
			}
			else {
				Vector2 pos = (Vector2)stream.ReceiveNext();
				interpolatedPosition.AddPosition(pos, info.timestamp);
			}
		}
	}
}