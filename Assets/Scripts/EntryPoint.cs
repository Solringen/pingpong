﻿using PingPong.Network;
using PingPong.UI;
using UnityEngine;

namespace PingPong
{
	public class EntryPoint : MonoBehaviour
	{
#pragma warning disable 0649
		[SerializeField] private OfflineGame offlineGame;
		[SerializeField] private NetworkGame networkGame;
		[SerializeField] private Referee referee;
		[SerializeField] private GameUI ui;
#pragma warning restore 0649

		private Game game;

		private void Start() {
			game = GetGameImplementation();
			SubscribeToReferee();
			ui.UpdateHighscore(referee.Highscore);

			game.StartGame();
		}

		private Game GetGameImplementation() {
			if (PhotonNetwork.connected) {
				return networkGame;
			}
			else {
				return offlineGame;
			}
		}

		private void SubscribeToReferee() {
			referee.Score1Changed += ui.UpdateScore1;
			referee.Score2Changed += ui.UpdateScore2;
			referee.HighscoreChanged += ui.UpdateHighscore;

			if (Host) {
				referee.Score1Changed += (score) => game.PlayerScored(1, score);
				referee.Score2Changed += (score) => game.PlayerScored(2, score);
			}
		}

		private bool Host => !PhotonNetwork.connected || PhotonNetwork.isMasterClient;
	}
}